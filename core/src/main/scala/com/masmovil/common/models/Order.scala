package com.masmovil.common.models

import java.util.UUID

case class OrderId(id: UUID)
case class Order(orderId: OrderId, customer: Customer, totalAmount: Double)

object Order {
  def create(customer: Customer, totalAmount: Double): Order =
    Order(OrderId(UUID.randomUUID()), customer, totalAmount)
}
