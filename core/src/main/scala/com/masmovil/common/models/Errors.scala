package com.masmovil.common.models

sealed trait Error

case class PhoneNotFound(phoneId: PhoneId) extends Error
case class ServiceNotAvailable(msg: String) extends Error
case object OrderCantBeEmpty extends Error
