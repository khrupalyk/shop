package com.masmovil.common.models

case class Customer(userName: String, surname: String, email: String)
