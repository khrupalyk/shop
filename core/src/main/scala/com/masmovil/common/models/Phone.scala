package com.masmovil.common.models

import java.util.UUID

case class PhoneId(id: UUID)
case class Phone(id: PhoneId, name: String, description: String, image: String, price: Double)
