package com.masmovil.providers

import com.typesafe.config.ConfigFactory
import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._

trait DBProvider {
  val config = ConfigFactory.load()

  val url = config.getString("movil.database.url")
  val user = config.getString("movil.database.user")
  val pass = config.getString("movil.database.password")

  val db: PostgresProfile.backend.DatabaseDef = {
    Class.forName("org.postgresql.Driver")

    Database.forURL(
      url = url,
      user = user,
      password = pass)
  }

}
