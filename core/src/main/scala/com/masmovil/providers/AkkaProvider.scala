package com.masmovil.providers

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

trait AkkaProvider {
  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
}
