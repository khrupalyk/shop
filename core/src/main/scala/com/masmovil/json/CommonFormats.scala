package com.masmovil.json

import java.util.UUID

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.masmovil.common.models._
import spray.json.{DefaultJsonProtocol, JsString, JsValue, RootJsonFormat}

trait CommonFormats extends SprayJsonSupport with DefaultJsonProtocol  {
  implicit val uuidFormats: RootJsonFormat[UUID] = new RootJsonFormat[UUID] {
    override def write(obj: UUID): JsValue = JsString(obj.toString)
    override def read(json: JsValue): UUID = json match {
      case JsString(str) => UUID.fromString(str)
    }
  }
  implicit val phoneIdFormat: RootJsonFormat[PhoneId] = new RootJsonFormat[PhoneId] {
    override def read(json: JsValue): PhoneId = PhoneId(uuidFormats.read(json))
    override def write(obj: PhoneId): JsValue = JsString(obj.id.toString)
  }

  implicit val orderIdFormat: RootJsonFormat[OrderId] = new RootJsonFormat[OrderId] {
    override def read(json: JsValue): OrderId = OrderId(uuidFormats.read(json))
    override def write(obj: OrderId): JsValue = JsString(obj.id.toString)
  }

  implicit val customerFormat: RootJsonFormat[Customer] = jsonFormat3(Customer)
  implicit val orderFormat: RootJsonFormat[Order] = jsonFormat3(Order.apply)
  implicit val phoneFormat: RootJsonFormat[Phone] = jsonFormat5(Phone)
}
