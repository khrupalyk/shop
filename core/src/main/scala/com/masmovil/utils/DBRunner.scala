package com.masmovil.utils

import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future

trait DBRunner {
  def run[T](action: DBIO[T]): Future[T]
}

class PostgresDBRunner(db: Database) extends DBRunner {

  def run[T](action: DBIO[T]): Future[T] = db.run(action)
}