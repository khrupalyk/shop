package com.masmovil.endpoints

import java.util.UUID

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.masmovil.common.models.{Phone, PhoneId}
import com.masmovil.http.AllFormats
import com.masmovil.http.models.FilterPhonesByIdsRequest
import com.masmovil.services.FindPhonesService
import org.scalatest.{FunSpec, Matchers}
import org.scalamock.scalatest.MockFactory
import spray.json._

import scala.concurrent.{ExecutionContext, Future}


class PhoneAPISpec extends FunSpec with ScalatestRouteTest with MockFactory with Matchers with AllFormats {

  val p1Id = PhoneId(UUID.randomUUID())
  val phone1 = Phone(p1Id, "p1", "d1", "link", 1)
  val p2Id = PhoneId(UUID.randomUUID())
  val phone2 = Phone(p2Id, "p2", "d2", "link", 3)
  val p3IdNotExistence = PhoneId(UUID.randomUUID())

  val findPhonesService = mock[FindPhonesService]

  val routes = PhonesAPI(findPhonesService).routes

  val requestWithAllIds = FilterPhonesByIdsRequest(Set(p2Id, p1Id))
  val requestWithOneOkAndOneNotExistenceId = FilterPhonesByIdsRequest(Set(p2Id, p1Id))

  it("should find all phones by ids") {
    (findPhonesService.findByIds(_: Set[PhoneId])(_: ExecutionContext)).expects(*, *)
      .returns(Future.successful(Seq(phone1, phone2)))
    Post("/phones/search", Some(requestWithAllIds.toJson)) ~> routes ~> check {
      status shouldBe StatusCodes.OK
    }
  }

  it("should find only one phone") {
    (findPhonesService.findByIds(_: Set[PhoneId])(_: ExecutionContext)).expects(*, *)
      .returns(Future.successful(Seq(phone1)))
    Post("/phones/search", Some(requestWithOneOkAndOneNotExistenceId.toJson)) ~> routes ~> check {
      status shouldBe StatusCodes.OK
    }
  }
}
