create table phones(
    id UUID PRIMARY KEY,
    name TEXT,
    description TEXT,
    image TEXT,
    price decimal
);