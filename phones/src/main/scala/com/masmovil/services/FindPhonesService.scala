package com.masmovil.services

import com.masmovil.common.models.{Phone, PhoneId}
import slick.jdbc.PostgresProfile.api._
import com.masmovil.database.schema._
import com.masmovil.utils.DBRunner

import scala.concurrent.{ExecutionContext, Future}

trait FindPhonesService {
  def find()(implicit ex: ExecutionContext): Future[Seq[Phone]]
  def findByIds(ids: Set[PhoneId])(implicit ex: ExecutionContext): Future[Seq[Phone]]
}

class FindPhonesInDBService(db: DBRunner) extends FindPhonesService {
  override def find()(implicit ex: ExecutionContext): Future[Seq[Phone]] = {
    db.run(phoneTable.result)
  }

  override def findByIds(ids: Set[PhoneId])(implicit ex: ExecutionContext): Future[Seq[Phone]] = {
    db.run(phoneTable.filter(_.id.inSet(ids)).result)
  }
}
