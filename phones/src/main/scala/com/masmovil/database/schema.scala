package com.masmovil.database

import java.util.UUID

import com.masmovil.common.models.{Phone, PhoneId}
import slick.jdbc.PostgresProfile.api._

object schema {

  implicit val phoneIdMapped = MappedColumnType.base[PhoneId, UUID](
    _.id, PhoneId(_)
  )

  class PhoneTable(tag: Tag) extends Table[Phone](tag, "phones"){
    def id = column[PhoneId]("id", O.PrimaryKey)
    def name = column[String]("name")
    def description = column[String]("description")
    def image = column[String]("image")
    def price = column[Double]("price")

    def * = (id, name, description, image, price) <> ((Phone.apply _).tupled, Phone.unapply)
  }

  object phoneTable extends TableQuery(new PhoneTable(_)) {
    def create(p: Phone) = this returning this += p
  }
}
