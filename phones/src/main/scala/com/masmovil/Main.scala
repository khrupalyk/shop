package com.masmovil

import java.util.UUID

import com.masmovil.services.FindPhonesInDBService
import org.flywaydb.core.Flyway
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import com.masmovil.common.models.{Phone, PhoneId}
import com.masmovil.endpoints.PhonesAPI
import com.masmovil.database.schema._
import com.masmovil.providers.{AkkaProvider, DBProvider}
import com.masmovil.utils.PostgresDBRunner
import org.flywaydb.core.internal.database.postgresql.PostgreSQLDatabase

object Main extends App with DBProvider with AkkaProvider {

  //Run db db.migration
  val flyway = new Flyway()
  flyway.setDataSource(url, user, pass)
  flyway.migrate()

  val dbRunner = new PostgresDBRunner(db)

  val findPhonesService = new FindPhonesInDBService(dbRunner)

  if(config.getBoolean("movil.generator.enable")) {
      1 to config.getInt("movil.generator.num-of-phones") foreach(i => {
        val phone = Phone(PhoneId(
          UUID.randomUUID()), randomStr, randomStr, randomStr, i)
        db.run(phoneTable.create(phone))
      })
  }

  val api = PhonesAPI(findPhonesService).routes

  val bindingFuture = Http().bindAndHandle(api, "localhost", 8080)

  def randomStr = UUID.randomUUID().toString

}
