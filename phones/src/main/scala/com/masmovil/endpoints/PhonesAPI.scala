package com.masmovil.endpoints

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.masmovil.http.AllFormats
import com.masmovil.http.models.FilterPhonesByIdsRequest
import com.masmovil.providers.AkkaProvider
import com.masmovil.services.FindPhonesService

class PhonesAPI(findPhonesService: FindPhonesService) extends AllFormats with AkkaProvider {

  val routes: Route = path("phones") {
    get {
      complete {
        findPhonesService.find().map(_.map(phoneFormat.write))
      }
    }
  } ~ path("phones" / "search") {
    post {
      entity(as[FilterPhonesByIdsRequest]) { cmd =>
        complete {
          findPhonesService.findByIds(cmd.ids).map(_.map(phoneFormat.write))
        }
      }
    }
  }
}

object PhonesAPI {
  def apply(findPhonesService: FindPhonesService): PhonesAPI = new PhonesAPI(findPhonesService)
}
