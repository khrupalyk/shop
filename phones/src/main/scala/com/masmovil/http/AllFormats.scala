package com.masmovil.http


import com.masmovil.http.models.FilterPhonesByIdsRequest
import com.masmovil.json.CommonFormats
import spray.json.RootJsonFormat

trait AllFormats extends CommonFormats {
  implicit val filterPhonesByIdsFormat: RootJsonFormat[FilterPhonesByIdsRequest] = jsonFormat1(FilterPhonesByIdsRequest)
}
