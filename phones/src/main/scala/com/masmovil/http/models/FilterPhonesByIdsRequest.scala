package com.masmovil.http.models

import com.masmovil.common.models.PhoneId

case class FilterPhonesByIdsRequest(ids: Set[PhoneId])
