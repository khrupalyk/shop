import sbt.Keys._
import sbt.{ModuleID, _}

object Dependencies {

  def dependencies(deps: ModuleID*): Seq[Setting[_]] = libraryDependencies ++= deps

  val akkaHttp = "com.typesafe.akka" %% "akka-http" % "10.1.1"
  val akkaActor = "com.typesafe.akka" %% "akka-actor" % "2.5.12"
  val akkaTest = "com.typesafe.akka" %% "akka-http-testkit" % "10.1.1" % Test
  val akkaStream = "com.typesafe.akka" %% "akka-stream" % "2.5.12"
  val akkaJson = "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.10"

  val postgres = "org.postgresql" % "postgresql" % "42.2.2.jre7"
  val slick = "com.typesafe.slick" %% "slick" % "3.2.1"
  val flydb = "org.flywaydb" % "flyway-maven-plugin" % "5.0.7"
  val logging = "org.slf4j" % "slf4j-api" % "1.7.5"
  val cats = "org.typelevel" %% "cats-core" % "1.0.1"
  val scalamock = "org.scalamock" %% "scalamock" % "4.1.0" % Test
  val scalatest = "org.scalatest" %% "scalatest" % "3.0.5" % Test

}
