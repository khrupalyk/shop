
name := "MasMovil"
version := "1.0"
scalaVersion := "2.12.6"

resolvers += "typesafe" at "http://repo.typesafe.com/typesafe/releases/"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

val commonSettings = Dependencies.dependencies(
  Dependencies.akkaActor,
  Dependencies.akkaHttp,
  Dependencies.akkaJson,
  Dependencies.akkaTest,
  Dependencies.akkaStream,
  Dependencies.flydb,
  Dependencies.postgres,
  Dependencies.slick,
  Dependencies.logging,
  Dependencies.cats,
  Dependencies.scalatest,
  Dependencies.scalamock
)

lazy val core = project
  .in(file("core"))
  .settings(commonSettings)

lazy val orders = project
  .in(file("orders"))
  .dependsOn(core)
  .settings(commonSettings)

lazy val phones = project
  .in(file("phones"))
  .dependsOn(core)
  .settings(commonSettings)

lazy val root = project
  .in(file("."))
  .aggregate(orders, phones, core)