package com.masmovil.http

import com.masmovil.http.models.{CreateOrder, Customer, ErrorResponse}
import com.masmovil.json.CommonFormats
import spray.json.RootJsonFormat

trait AllFormats extends CommonFormats {
  implicit val httpCustomerFormat: RootJsonFormat[Customer] = jsonFormat3(Customer)
  implicit val createOrderFormat: RootJsonFormat[CreateOrder] = jsonFormat2(CreateOrder)
  implicit val errorResponseFormat: RootJsonFormat[ErrorResponse] = jsonFormat1(ErrorResponse)
}
