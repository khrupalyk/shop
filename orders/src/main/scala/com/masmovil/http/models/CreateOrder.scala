package com.masmovil.http.models

import com.masmovil.common.models.PhoneId

case class Customer(userName: String, surname: String, email: String)
case class CreateOrder(customer: Customer, phoneIds: Seq[PhoneId])
