package com.masmovil.endpoints

import akka.http.scaladsl.server.Directives._
import com.masmovil.http.models.{ErrorResponse, CreateOrder => HttpCreateOrder}
import com.masmovil.services.{CreateOrderService, FindOrderService}
import com.masmovil.services.CreateOrderService.CreateOrder
import OrdersAPI._
import akka.http.scaladsl.model.StatusCodes
import com.masmovil.common.models._
import com.masmovil.http.AllFormats
import com.masmovil.providers.AkkaProvider
import spray.json._

class OrdersAPI(
  createOrderService: CreateOrderService,
  findOrderService: FindOrderService) extends AkkaProvider with AllFormats {

  val routes = path("orders") {
    post {
      entity(as[HttpCreateOrder]) { action =>
        complete {
          createOrderService.createOrder(map(action)) map {
            case Right(order) => StatusCodes.OK -> order.toJson
            case Left(PhoneNotFound(id)) => StatusCodes.NotFound -> ErrorResponse(s"Phone with id ${id.id} not found").toJson
            case Left(ServiceNotAvailable(msg)) => StatusCodes.InternalServerError -> ErrorResponse(msg).toJson
            case Left(OrderCantBeEmpty) => StatusCodes.BadRequest -> ErrorResponse("Order can't be empty!").toJson
          }
        }
      }
    }
  } ~ path ("orders" / JavaUUID) { orderId =>
    get {
      complete {
        findOrderService.find(OrderId(orderId)).map {
          case Some(order) => StatusCodes.OK -> order.toJson
          case None => StatusCodes.NotFound -> ErrorResponse("Order not found").toJson
        }
      }
    }
  }


}

object OrdersAPI {
  def apply(createOrderService: CreateOrderService, findOrderService: FindOrderService): OrdersAPI =
    new OrdersAPI(createOrderService, findOrderService)

  def map(http: HttpCreateOrder): CreateOrder =
    CreateOrder(
      Customer(
        http.customer.userName,
        http.customer.surname,
        http.customer.email),
      http.phoneIds
    )
}
