package com.masmovil

import java.util.UUID

import akka.http.scaladsl.Http
import com.masmovil.endpoints.OrdersAPI
import com.masmovil.providers.{AkkaProvider, DBProvider}
import com.masmovil.services.{CreateOrderInDbService, FindOrderInDbService, FindPhonesServiceImpl}
import com.masmovil.utils.PostgresDBRunner
import org.flywaydb.core.Flyway

object Main extends App with DBProvider with AkkaProvider {
  //Run db db.migration
  val flyway = new Flyway()
  flyway.setDataSource(url, user, pass)
  flyway.migrate()

  val dbRunner = new PostgresDBRunner(db)

  val findPhonesService = new FindPhonesServiceImpl(config)
  val findOrderService = new FindOrderInDbService(dbRunner)
  val createOrderService = new CreateOrderInDbService(dbRunner, findPhonesService)

  val api = OrdersAPI(createOrderService, findOrderService).routes

  val bindingFuture = Http().bindAndHandle(api, "localhost", 8081)

  def randomStr = UUID.randomUUID().toString
}
