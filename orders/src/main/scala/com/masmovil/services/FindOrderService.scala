package com.masmovil.services

import com.masmovil.common.models.{Order, OrderId}
import slick.jdbc.PostgresProfile.api._
import com.masmovil.database.schema._
import com.masmovil.utils.DBRunner

import scala.concurrent.{ExecutionContext, Future}

trait FindOrderService {
  def find(orderId: OrderId)(implicit ec: ExecutionContext): Future[Option[Order]]
}

class FindOrderInDbService(db: DBRunner) extends FindOrderService {
  override def find(orderId: OrderId)(implicit ec: ExecutionContext): Future[Option[Order]] = {
    db.run(orderTable.filter(o => o.id === orderId).take(1).result).map(_.headOption.map(_.toOrder))
  }
}
