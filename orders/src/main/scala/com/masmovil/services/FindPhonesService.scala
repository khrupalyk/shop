package com.masmovil.services

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import com.masmovil.common.models._
import com.typesafe.config.Config
import spray.json.{DefaultJsonProtocol, JsArray, JsValue, RootJsonReader}
import akka.http.scaladsl.unmarshalling._
import com.masmovil.http.AllFormats
import com.masmovil.providers.AkkaProvider
import spray.json._

import scala.concurrent.Future

trait FindPhonesService {
  def find(phonesIds: Seq[PhoneId]): Future[Either[Error, Seq[Phone]]]
}

class FindPhonesServiceImpl(config: Config) extends FindPhonesService with AkkaProvider {

  import FindPhonesService._

  private val phonesUrl = config.getString("movil.phone-service.search-phones-url")

  override def find(phonesIds: Seq[PhoneId]): Future[Either[Error, Seq[Phone]]] = {

    val response = Http().singleRequest(
      HttpRequest(
        HttpMethods.POST,
        uri = phonesUrl,
        entity = HttpEntity(FindPhones(phonesIds).toJson(findPhonesFormat).toString).withContentType(MediaTypes.`application/json`))

    )

    response.flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        Unmarshal(entity).to[String].map(str => validateProductExistence(phonesIds, parseJson(str)))
      case HttpResponse(status, _, entity, _) =>
        Future.successful(Left(ServiceNotAvailable(s"Error when call phone service. status = $status, response = ${entity.toString}")))
    }
  }

  private def validateProductExistence(phonesIds: Seq[PhoneId], phones: Seq[Phone]) = {
    phonesIds.find(pid => !phones.exists(_.id == pid)).fold[Either[PhoneNotFound, Seq[Phone]] ](Right(phones)) {
      p => Left(PhoneNotFound(p))
    }
  }


}

object FindPhonesService extends DefaultJsonProtocol with AllFormats {

  case class FindPhones(ids: Seq[PhoneId])
  case class PhonesList(phones: Seq[Phone])

  implicit val findPhonesFormat = jsonFormat1(FindPhones)
  implicit val phonesListFormat = new RootJsonReader[PhonesList] {
    override def read(json: JsValue): PhonesList = json match {
      case JsArray(elems) => PhonesList(elems.map(phoneFormat.read))
    }
  }

  def parseJson(str: String): List[Phone] = {
    str.parseJson.convertTo[List[Phone]]
  }
}