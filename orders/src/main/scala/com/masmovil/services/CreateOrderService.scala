package com.masmovil.services

import cats.data.EitherT
import com.masmovil.common.models.{Order, Phone, PhoneId}
import slick.jdbc.PostgresProfile.api._
import com.masmovil.database.schema._
import com.masmovil.common.models._
import com.masmovil.utils.{DBRunner, StrictLogging}
import cats.instances.future._
import com.masmovil.database.records.OrderRecord

import scala.concurrent.{ExecutionContext, Future}

trait CreateOrderService {
  def createOrder(cmd: CreateOrderService.CreateOrder)(implicit ex: ExecutionContext): Future[Either[Error, Order]]
}

class CreateOrderInDbService(db: DBRunner, findPhonesService: FindPhonesService) extends CreateOrderService with StrictLogging {
  override def createOrder(cmd: CreateOrderService.CreateOrder)(implicit ex: ExecutionContext): Future[Either[Error, Order]] = {
    (for {
      _ <- EitherT(validateEmptyOrder(cmd.phoneIds))
      phones <- EitherT(findPhonesService.find(cmd.phoneIds))
      result <-EitherT( {
        val order = Order.create(cmd.customer, calculateTotal(phones))
        println(s"Create order $order with phones $phones")
        saveOrder(order)
      })
    } yield result).value
  }

  private def calculateTotal(phones: Seq[Phone]): Double = phones.map(_.price).sum

  private def saveOrder(order: Order)(implicit ex: ExecutionContext): Future[Either[Error, Order]] = {
    db.run(orderTable.create(OrderRecord.fromOrder(order))).map(_ => Right(order))
  }

  private def validateEmptyOrder(phonesId: Seq[PhoneId]) =
    Future.successful(if(phonesId.isEmpty) Left(OrderCantBeEmpty) else Right(phonesId))
}


object CreateOrderService {
  case class CreateOrder(
    customer: Customer,
    phoneIds: Seq[PhoneId])
}