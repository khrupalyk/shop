package com.masmovil.database

import java.util.UUID

import com.masmovil.common.models.{Order, OrderId}
import com.masmovil.database.records.OrderRecord
import slick.jdbc.PostgresProfile.api._

object schema {
  implicit val orderIdMapped = MappedColumnType.base[OrderId, UUID](
    _.id, OrderId
  )

  class OrderTable(tag: Tag) extends Table[OrderRecord](tag, "orders"){
    def id = column[OrderId]("id", O.PrimaryKey)
    def userName = column[String]("user_name")
    def surname = column[String]("surname")
    def email = column[String]("email")
    def totalAmount = column[Double]("total_amount")

    def * = (id, userName, surname, email, totalAmount) <> ((OrderRecord.apply _).tupled, OrderRecord.unapply)
  }

  object orderTable extends TableQuery(new OrderTable(_)) {
    def create(p: OrderRecord) = this returning this += p
  }
}
