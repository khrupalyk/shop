package com.masmovil.database.records

import com.masmovil.common.models.{Customer, Order, OrderId}

case class OrderRecord(orderId: OrderId, userName: String, surname: String, email: String, totalAmount: Double) {
  def toOrder = Order(
    orderId,
    Customer(userName, surname, email),
    totalAmount
  )
}

object OrderRecord {
  def fromOrder(order: Order): OrderRecord =
    new OrderRecord(order.orderId, order.customer.userName, order.customer.surname, order.customer.email, order.totalAmount)
}
