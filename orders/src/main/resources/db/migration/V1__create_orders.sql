create table orders(
    id UUID PRIMARY KEY,
    user_name TEXT,
    surname TEXT,
    email TEXT,
    total_amount decimal
);