package com.musmovil.services

import java.util.UUID

import com.masmovil.common.models.{Order, OrderId}
import com.masmovil.database.records.OrderRecord
import com.masmovil.services.{FindOrderInDbService, FindOrderService}
import com.masmovil.utils.DBRunner
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FunSpec, Matchers}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import slick.jdbc.PostgresProfile.api._
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

class FindOrderServiceSpec extends FunSpec with MockFactory with ScalaFutures with Matchers {

  val dbRunner = mock[DBRunner]

  val orderId = OrderId(UUID.randomUUID())
  val orderRecord = OrderRecord(orderId, "" ,"" ,"", 1)

  implicit val defaultPatience = PatienceConfig(timeout = Span(20, Seconds), interval = Span(500, Millis))

  it("should return order") {

    (dbRunner.run(_: DBIO[Any])).expects(*).returns(Future.successful(Seq(orderRecord)))
    val findOrderService = new FindOrderInDbService(dbRunner)

    whenReady(findOrderService.find(orderId)) {
      _ shouldBe Some(orderRecord.toOrder)
    }
  }

  it("should return None if order not exist") {

    (dbRunner.run(_: DBIO[Any])).expects(*).returns(Future.successful(Seq()))
    val findOrderService = new FindOrderInDbService(dbRunner)

    whenReady(findOrderService.find(orderId)) {
      _ shouldBe None
    }
  }

}
