package com.musmovil.services

import java.util.UUID

import com.masmovil.common.models._
import com.masmovil.database.records.OrderRecord
import com.masmovil.services.{CreateOrderInDbService, CreateOrderService, FindPhonesService}
import com.masmovil.utils.DBRunner
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{FunSpec, Matchers}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class CreateOrderService extends FunSpec with Matchers with MockFactory with ScalaFutures {

  val dbRunner = mock[DBRunner]
  val phoneService = mock[FindPhonesService]
  val customer = Customer("n", "s", "e@e.com")
  val pid = PhoneId(UUID.randomUUID())
  val phoneIds = Seq(pid)
  val cmd = CreateOrderService.CreateOrder(customer, phoneIds)
  val emptyOrderCmd = CreateOrderService.CreateOrder(customer, Seq.empty)
  val orderId = OrderId(UUID.randomUUID())
  val phone = Phone(pid, "name", "desc", "img", 2)
  val order = Order(orderId, customer, 2)
  val orderRecord = OrderRecord.fromOrder(order)

  implicit val defaultPatience = PatienceConfig(timeout = Span(20, Seconds), interval = Span(500, Millis))


  it("should create order") {
    val service = new CreateOrderInDbService(dbRunner, phoneService)

    (phoneService.find _).expects(phoneIds).returns(Future.successful(Right(Seq(phone))))
    (dbRunner.run(_: DBIO[Any])).expects(*).returns(Future.successful(orderRecord))

    whenReady(service.createOrder(cmd)) { createdOrderE =>
      val createdOrder = createdOrderE.right.get
      createdOrder.totalAmount shouldBe order.totalAmount
      createdOrder.customer shouldBe order.customer
    }
  }

  it("should return PhoneNotFound") {
    val service = new CreateOrderInDbService(dbRunner, phoneService)

    (phoneService.find _).expects(phoneIds).returns(Future.successful(Left(PhoneNotFound(pid))))

    whenReady(service.createOrder(cmd)) {
      _ shouldBe Left(PhoneNotFound(pid))
    }
  }

  it("should return OrderCantBeEmpty") {
    val service = new CreateOrderInDbService(dbRunner, phoneService)

    whenReady(service.createOrder(emptyOrderCmd)) {
      _ shouldBe Left(OrderCantBeEmpty)
    }
  }

}
