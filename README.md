# Run services

1. Run `sbt dist` from project root directory.
2. After artifacts are packaged move them to same directory with `rm -rf releases && mkdir -p releases && cp */target/universal/*.zip releases && cd releases`.
3. Unzip artifacts `unzip "*.zip"`
4. Go to `phones-<version>` and edit file `application.conf`. Do the same for orders. Go to `orders-<version>/conf` and edit file `application.conf`.
5. Run services using
`./phones-<version>/bin/phones -Dconfig.file=./phones-<version>/conf/application.conf` for phones service and`./orders-<version>/bin/orders -Dconfig.file=./orders-<version>/conf/application.conf` for orders service

## How to use the system
- curl -XPOST 'localhost:8081/orders' -d '{"customer":{ "email":"em@gmail.com", "userName":"Andrii", "surname": "Andrii"}, "phoneIds":["04637baf-266c-471b-9254-fa8ae4742c8b"]}' -H 'Content-type: application/json'
- curl -XGET 'localhost:8080/phones'

# Answers

>How would you improve the system?

1. Move all http interaction to another service.
2. Add integration tests.
3. Use compiled query for speedup work with database.
4. Create index on db tables for speedup search.
5. Validate input parameters/phone count before make the order.
6. Add additional layout for convert service responses to HTTP responses.
7. Make http layout more clear by introduce request/response types.
8. Introduce circuit breaker pattern for prevent DoS attack to phone service.

>How would you avoid your order api to be overflow?

1. Microservice can be scale up or down by deploying on demand instances which are balanced via load balancer.
2. If bottleneck is database, we can switch to NoSQL solution for database scaling.